package main

import (
	"bitbucket.org/wingolab/linkage"
	"gopkg.in/urfave/cli.v1" // imports as package "cli"
	"log"
	"os"
)

func main() {
	app := cli.NewApp()
	app.Name = "linkage2Snp"
	app.Authors = []cli.Author{cli.Author{Name: "Thomas S. Wingo", Email: "thomas.wingo@emory.edu"}}
	app.Usage = "Convert binary linkage to snp file"
	app.Version = "0.0.1"
	app.Commands = []cli.Command{
		{
			Name:  "convert",
			Usage: "Convert binary linkage to snp format.",
			Flags: []cli.Flag{
				&cli.StringFlag{Name: "bed", Usage: "bed file"},
				&cli.StringFlag{Name: "bim", Usage: "bim file"},
				&cli.StringFlag{Name: "fam", Usage: "fam file"},
				&cli.StringFlag{Name: "two", Usage: "2bit file"},
				&cli.StringFlag{Name: "out, o", Usage: "output file"},
			},
			Action: func(c *cli.Context) {
				Linkage2Snp(c.String("bed"), c.String("bim"), c.String("fam"),
					c.String("two"), c.String("out"))
			},
		},
	}

	app.Run(os.Args)
}

func Linkage2Snp(BedFile, BimFile, FamFile, TwoBitFile, OutName string) {

	if (len(BedFile)) == 0 {
		log.Fatalln("Please provide bed file (.bed)")
	}
	if (len(BimFile)) == 0 {
		log.Fatalln("Please provide bim file (.bim)")
	}
	if (len(FamFile)) == 0 {
		log.Fatalln("Please provide fam file (.fam)")
	}
	if (len(TwoBitFile)) == 0 {
		log.Fatalln("Please provide 2bit genome file (.2bit)")
	}
	if (len(OutName)) == 0 {
		log.Fatalln("Please provide output file")
	}

	// process genetic site data
	sites := linkage.LoadBimFile(BimFile, TwoBitFile)
	log.Printf("sites: %d\n", len(sites))

	// process pedigree data
	ppl := linkage.LoadFamFile(FamFile, len(sites))
	log.Printf("samples: %d\n", len(ppl))

	// masks for reading binary bed file
	masks := []byte{0x03, 0x0c, 0x30, 0xC0}
	linkage.LoadBedFile(BedFile, masks, sites, ppl)

	// write a snp file
	Sep := "\t"
	linkage.WriteSnpFile(OutName, Sep, sites, ppl)
}
