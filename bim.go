package linkage

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"regexp"
	"sort"
	"strconv"
	"strings"
)

type Call byte

type Site struct {
	Chrom       string  "Chr"
	Name        string  "MarkerName"
	Cm          string  "cM"
	Bp          int     "Pos"
	A1          string  "AlleleName1"
	A2          string  "AlleleName2"
	HetGeno     string  "Genotype"
	MAF         float32 "MAF"
	Alleles     string  "AllAlleles"
	AlleleCount string  "AlleleCount"
	Type        string  "Type"
	Ref         string  "Reference"
	Concord     bool    "Concordant"
}

type Allele struct {
	Name  string
	Count int
}
type ByAlleleCount []Allele

func (a ByAlleleCount) Len() int           { return len(a) }
func (a ByAlleleCount) Swap(i, j int)      { a[i], a[j] = a[j], a[i] }
func (a ByAlleleCount) Less(i, j int) bool { return a[i].Count < a[j].Count }

func LoadBimFile(file string, TwoBitFile string) []Site {
	// counters for concordance between bim file and reference
	var CountDiscord int

	// load twobit
	genome := LoadTwoBit(TwoBitFile)

	// storage for experiment
	sites := make([]Site, 0)

	// process bim file
	f, err := os.Open(file)
	if err != nil {
		log.Fatalf("Cannot open bim file '%s': %s", file, err)
	}
	defer f.Close()

	scanner := bufio.NewScanner(f)

	for scanner.Scan() {
		line := scanner.Text()
		fields := strings.Fields(line)
		s, err := makeSite(fields, &genome)
		if err != nil {
			log.Fatal(err)
		}
		if s.Concord == false {
			CountDiscord++
		}
		sites = append(sites, s)
	}

	FractDiscord := float32(CountDiscord) / float32(len(sites))

	log.Printf("%d of %d (%0.2f) discordant sites\n", CountDiscord, len(sites), FractDiscord)
	return sites
}

// UpdateSite() is used to update the Site struct with data gathered from
// examining genotypes from all individuals in the experiment
func UpdateSite(s *Site, AlleleCounts map[string]int) {

	alleles := make([]Allele, 0)
	tot := 0

	for allele, val := range AlleleCounts {
		// include all non-missing alleles
		if allele != "0" {
			alleles = append(alleles, Allele{Name: allele, Count: val})
			tot += val
		}
	}
	AlleleCount := len(alleles)

	// assign data to site; first deal with 2 special cases:
	// 1) when we only observed missing data
	// 2) when we find only one allele (usually reference + missing data)
	if AlleleCount == 0 {
		s.Alleles = s.Ref
		s.AlleleCount = "0"
	} else if AlleleCount == 1 {
		// add reference if not already alleles[0].Name
		if alleles[0].Name == s.Ref {
			s.Alleles = alleles[0].Name
			s.AlleleCount = strconv.Itoa(tot)
		} else {
			s.Alleles = strings.Join([]string{alleles[0].Name, s.Ref}, ",")
			s.AlleleCount = strings.Join([]string{strconv.Itoa(tot), "0"}, ",")
		}
	} else {
		sort.Sort(ByAlleleCount(alleles))

		// determine type and set alleles
		AllAlleles := make([]string, 0)
		AllAlleleCounts := make([]string, 0)

		for _, a := range alleles {
			AllAlleles = append(AllAlleles, a.Name)
			AllAlleleCounts = append(AllAlleleCounts, strconv.Itoa(a.Count))
		}

		// save all alleles and their counts for the structure
		s.Alleles = strings.Join(AllAlleles, ",")
		s.AlleleCount = strings.Join(AllAlleleCounts, ",")
	}
}

// makeSite takes fileds from a bim file and creates a Site struct
func makeSite(fields []string, g *Genome) (Site, error) {
	s := new(Site)

	// capture valid chromosome names
	// the regex captures 2 things - the 1st part variably captures "chr" and
	// everything to the end of the line and the 2nd part captures the
	// trailing chromosome name (e.g., "X"); it will always capture 2 things
	// when used with FindStringSubmatch()
	re := regexp.MustCompile(`(?:[CHR]{0,3}_*)([A-Z|0-9]+)$`)
	matches := re.FindStringSubmatch(strings.ToUpper(fields[0]))
	if matches == nil {
		return *s, fmt.Errorf("Unknown chromosome: %s", fields[0])
	}

	// we are selecting the match for the trailing chromsome name without "chr"
	chr := matches[1]

	// convert sex and mitochondrial chromosomes
	// see: http://pngu.mgh.harvard.edu/~purcell/plink/data.shtml, MAP files section
	//      for coding instructions
	if chr == "23" {
		chr = "X"
	} else if chr == "24" {
		chr = "Y"
	} else if chr == "25" {
		chr = "XY"
	} else if chr == "26" || chr == "MT" {
		chr = "M"
	}

	// add chr to chromosome number
	s.Chrom = "chr" + chr
	s.Name = fields[1]
	s.Cm = fields[2]
	bp, err := strconv.ParseInt(fields[3], 10, 64)
	if err != nil {
		return *s, fmt.Errorf("Error converting position %s to a number", fields[3])
	}
	s.Bp = int(bp)
	s.A1 = fields[4]
	s.A2 = fields[5]

	s.Ref, err = g.RefBase(s.Chrom, s.Bp)
	if err != nil {
		return *s, fmt.Errorf("Error finding base for %s:%d: %s", s.Chrom, s.Bp, err)
	}

	if s.Ref == s.A1 {
		s.Concord = true
	}

	// assign snp/ins/del status
	// update A1 and A2 for indels
	if IsBase(s.A1) && IsBase(s.A2) {
		s.Type = "SNP"
	} else if s.A1 == "-" {
		s.Type = "INS"
		s.A1 = s.Ref
		s.A2 = "+" + s.A2
	} else if s.A2 == "-" {
		s.Type = "DEL"
		s.A1 = s.Ref
		s.A2 = "-" + strconv.Itoa(len(s.A2))
	} else {
		s.Type = "MESS"
		s.A1 = s.Ref
		s.A2 = "NA"
	}
	s.Alleles = strings.Join([]string{s.A1, s.A2}, ",")

	if s.Type == "SNP" {
		s.HetGeno = LookUpHetGeno(s.A1, s.A2)
	}
	return *s, nil
}

func LookUpHetGeno(a string, b string) string {

	switch {
	case a == "A" && b == "C" || b == "A" && a == "C":
		return "M"
	case a == "A" && b == "G" || b == "A" && a == "G":
		return "R"
	case a == "A" && b == "T" || b == "A" && a == "T":
		return "W"
	case a == "C" && b == "G" || b == "C" && a == "G":
		return "S"
	case a == "C" && b == "T" || b == "C" && a == "T":
		return "Y"
	case a == "G" && b == "T" || b == "G" && a == "T":
		return "K"
	}
	return ""
}

// IsBase returns true for real bases
func IsBase(a string) bool {
	switch a {
	case "A", "C", "G", "T", "a", "c", "g", "t":
		return true
	default:
		return false
	}
}
