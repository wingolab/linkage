package linkage

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"strings"
)

type Person struct {
	FamId string "FamilyId"
	Id    string "ReserchId"
	MatId string "MaternalId"
	PatId string "PaternalId"
	Sex   string "Sex"
	Aff   string "AffectationStatus"
	Calls []Call // [] indexed as the Ex.Sites array
}

func LoadFamFile(file string, SiteCount int) []Person {

	ppl := make([]Person, 0)

	// process fam file
	f, err := os.Open(file)
	if err != nil {
		log.Fatalf("Cannot open open fam file '%s': %s", file, err)
	}
	defer f.Close()

	scanner := bufio.NewScanner(f)

	for scanner.Scan() {
		line := scanner.Text()
		fields := strings.Fields(line)
		p, err := makePerson(fields, SiteCount)
		if err != nil {
			log.Fatalf("Error processing fam file '%s': %s", file, err)
		}
		ppl = append(ppl, p)
	}
	return ppl
}

// makePerson takes fields from a fam file and creates a Person struct
func makePerson(fields []string, SiteCount int) (Person, error) {
	p := new(Person)

	if len(fields) != 6 {
		return *p, fmt.Errorf("Wrong number of fields in fam file; expected 6 found %d", len(fields))
	}

	p.FamId = fields[0]
	p.Id = fields[1]
	p.MatId = fields[2]
	p.PatId = fields[3]
	p.Sex = fields[4]
	p.Aff = fields[5]
	p.Calls = make([]Call, SiteCount)

	return *p, nil
}
