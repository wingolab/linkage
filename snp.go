package linkage

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"strconv"
	"strings"
)

// SnpFileString() prints preamble for snpfile site
func SnpFileHeader(s Site) []string {
	return []string{s.Chrom, strconv.Itoa(s.Bp), s.Ref, s.Alleles, s.AlleleCount, s.Type}
}

// WriteSnpFile writes a snpfile to a given basename using the supplied sites
// array for a given array of Person; the separator is set with 'sep'
func WriteSnpFile(baseName string, sep string, sites []Site, ppl []Person) error {

	snpFileName := fmt.Sprintf("%s.snp", baseName)

	sFh, err := os.Create(snpFileName)
	if err != nil {
		log.Fatalf("Error writing %s: %s", snpFileName, err)
	}
	defer sFh.Close()

	w := bufio.NewWriter(sFh)

	// Generate the header line (i.e., top line)
	header := []string{"Fragment", "Position", "Reference", "Alleles", "Allele_Counts", "Type"}
	headerStr := strings.Join(header, sep)

	people := make([]string, 0, (2 * len(ppl)))
	for _, p := range ppl {
		people = append(people, strings.Join([]string{p.FamId, p.Id}, "-"))
	}
	// recall there are 2 separaters between individuals b/c of the geno and prob columns
	doubleSep := sep + sep
	peopleStr := strings.Join(people, doubleSep)

	headerStr = strings.Join([]string{headerStr, peopleStr}, sep)
	fmt.Fprintln(w, headerStr)

	for i, s := range sites {
		// only print certain types that have well defined hom and het states
		if s.Type == "DEL" || s.Type == "INS" || s.Type == "SNP" {
			genos := make([]string, 0)

			// add the snp site information preamble to line
			snpHeader := SnpFileHeader(s)
			for _, field := range snpHeader {
				genos = append(genos, field)
			}

			// add genotype for each person
			for _, ppl := range ppl {
				geno, err := GenoByteToGenoStr(ppl.Calls[i], s)
				if err != nil {
					log.Fatalf("Error Decoding Genotype: %s", err)
				}
				// append the genotype call and the probablity, which I'm assiging to 1
				genos = append(genos, geno, "1")
			}
			outStr := strings.Join(genos, sep)
			fmt.Fprintln(w, outStr)
		}
	}

	return w.Flush()
}
