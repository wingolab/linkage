linkage - Converts Binary Linkage to Snp File
---

# About

linkage is a Go library for reading binary linkage files as made by plink[1].

# Install

Fetch from github:

    go get install bitbucket.org/wingolab/linkage

Install command line tool:

    go install bitbucket.org/wingolab/linkage/cmd/linkage2Snp

# Usage

    linkage2Snp convert --bed file.bed --bim file.bim --fam file.fam --two hg19.2bit --out SnpFile.snp

# References

[1] http://pngu.mgh.harvard.edu/~purcell/plink/


