package linkage

import (
	"fmt"
	"io/ioutil"
	"log"
	"math"
	"os"
)

func LoadBedFile(file string, masks []byte, sites []Site, ppl []Person) {
	// set people and site counts
	pplCount := len(ppl)
	siteCount := len(sites)

	// size of data for a site is the number of people divided by 4 rounded to
	// the next whole number following the documentation here:
	// 		http://pngu.mgh.harvard.edu/~purcell/plink/binary.shtml
	blockSize := int(math.Ceil(float64(pplCount) / float64(4)))

	f, err := os.Open(file)
	if err != nil {
		log.Fatalf("Cannot open bedfile '%s': %s", file, err)
	}
	dat, err := ioutil.ReadAll(f)
	if err != nil {
		log.Fatalf("Cannot read bedfile '%s': %s", file, err)
	}

	// check magic number
	// should use encoding.binary package to do this
	if dat[0] != 0x6c || dat[1] != 0x1b || dat[2] != 0x01 {
		log.Fatalf("Error: not a binary plink file")
	}

	// i == siteIndex
	for i := 0; i < siteCount; i++ {

		// range over which we'll read the genotypes for all ppl
		from := 3 + blockSize*i
		to := 3 + blockSize*(i+1)
		pplIndex := 0

		// map of all alleles
		CntOfAlleles := make(map[string]int)

		for j := from; j < to; j++ {
			b := dat[j]
			var step uint
			step = 0
			for _, mask := range masks {

				if pplIndex < pplCount {

					bit := (b & mask) >> step

					alleles, err := GenoByteToAlleles(Call(bit), sites[i])
					if err != nil {
						log.Fatalf("Error decoding bed file: %s", err)
					}
					// fmt.Printf("%08b, % x, %08b, %08b, %d, %v, %v, %v, %v\n", b, b, mask, bit, step, alleles, j, i, pplIndex)
					for _, val := range alleles {
						if val != "-9" {
							// make a map for each site, as needed,
							// and count alleles for the site
							CntOfAlleles[val]++
						}
					}

					// assign call at site 's' with index 'i' to the individual must convert
					// the byte 'b' to a Call (which is also a byte) with 'Call(b)'
					ppl[pplIndex].Calls[i] = Call(bit)

					step += 2
					pplIndex++
				}
			}
		}
		// update the underlying site with the following - major allele, minor
		// allele, minor allele frequency,
		UpdateSite(&sites[i], CntOfAlleles)
	}
}

// GenoByteToAlleles takes a Call and Site and returns a string of alleles
// of either "0" (for missing) or S.A1 or S.A2
func GenoByteToAlleles(b Call, s Site) (alleles []string, err error) {

	if b == 0x00 {
		alleles = []string{s.A1, s.A1}
	} else if b == 0x01 {
		alleles = []string{"0", "0"}
	} else if b == 0x02 {
		alleles = []string{s.A1, s.A2}
	} else if b == 0x03 {
		alleles = []string{s.A2, s.A2}
	} else {
		err = fmt.Errorf("Imposible genotype: %x", b)
	}
	return alleles, err
}

// GenoByteToGenoStr takes a Call and Site and returns a genotype string using
// the appropriate snpfile nomenclature for each site.Type
func GenoByteToGenoStr(b Call, s Site) (string, error) {
	if s.Type == "SNP" {
		switch b {
		case 0x00:
			return s.A1, nil
		case 0x01:
			return "N", nil
		case 0x02:
			return s.HetGeno, nil
		case 0x03:
			return s.A2, nil
		}
	} else if s.Type == "INS" {
		switch b {
		case 0x00:
			return s.Ref, nil
		case 0x01:
			return "N", nil
		case 0x02:
			return "H", nil
		case 0x03:
			return "I", nil
		}
	} else if s.Type == "DEL" {
		switch b {
		case 0x00:
			return s.Ref, nil
		case 0x01:
			return "N", nil
		case 0x02:
			return "E", nil
		case 0x03:
			return "D", nil
		}
	}
	return "N", fmt.Errorf("Imposible genotype with byte: %08b for site: %s", b, s)
}
