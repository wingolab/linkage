package linkage

import (
	"fmt"
	"github.com/wingolab/twobit"
	"log"
	"os"
	"strings"
)

type Genome struct {
	assembly map[string]string
	chrCount int
}

func (g *Genome) RefBase(chr string, pos int) (string, error) {
	seq, ok := g.assembly[chr]
	if ok {
		if pos < len(seq) {
			base := seq[pos-1 : pos]
			return strings.ToUpper(string(base)), nil
		} else {
			return "", fmt.Errorf("Base, %d, beyond end of %s", pos, chr)
		}
	} else {
		return "", fmt.Errorf("Did not find chrom: %s", chr)
	}
}

func LoadTwoBit(twoBitFile string) Genome {

	// load 2bit file
	twoBitFh, err := os.Open(twoBitFile)
	if err != nil {
		log.Fatalf("Cannot open open fam file '%s': %s", twoBitFile, err)
	}
	defer twoBitFh.Close()
	tb, err := twobit.NewReader(twoBitFh)

	chrs := tb.Names()
	assembly := make(map[string]string, len(chrs))

	for _, chr := range chrs {
		seq, err := tb.Read(chr)
		if err != nil {
			log.Fatalf("Error reading 2bit file: %s", err)
		}
		assembly[chr] = string(seq)
		log.Printf("loaded %d bases for chr: %s", len(seq), chr)
	}

	return Genome{assembly: assembly, chrCount: len(chrs)}
}
